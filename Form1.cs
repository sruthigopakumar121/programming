﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace WindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-1NO43GM\SQLEXPRESS;Initial Catalog=demodatabase;Integrated Security=True");
        public int StuId;
        private void Form1_Load(object sender, EventArgs e)
        {
            GetStudentRecord();

        }

        private void GetStudentRecord()
        {
            
            SqlCommand cmd = new SqlCommand("Select * from demotable", con);
            DataTable dt = new DataTable();
            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();
            dataGridView1.DataSource = dt;
        }
           

        private void button4_Click(object sender, EventArgs e)
        {
            ResetFormControls();
        }

        private void ResetFormControls()
        {
            StuId = 0;
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
            comboBox1.Text = "";
            textBox1.Focus();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
          if(Isvalid())

            {

                SqlCommand cmd = new SqlCommand("INSERT INTO demotable VALUES (@name,@fathername,@age,@gender,@address,@phone)", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@name", textBox1.Text);
                cmd.Parameters.AddWithValue("@fathername", textBox2.Text);
                cmd.Parameters.AddWithValue("@age", textBox3.Text);
                cmd.Parameters.AddWithValue("@gender", comboBox1.Text);
                cmd.Parameters.AddWithValue("@address", textBox4.Text);
                cmd.Parameters.AddWithValue("@phone", textBox5.Text);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show(" New student is successfully saved in the dasebase ", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                GetStudentRecord();
                ResetFormControls();
            }

        }

        private bool Isvalid()
        {
            if (textBox1.Text == String.Empty)
            {
                MessageBox.Show("Please enter the Name of the Student ", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            StuId = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value);
            textBox1.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            textBox2.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            textBox3.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            comboBox1.Text = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();
            textBox4.Text = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();
            textBox5.Text = dataGridView1.SelectedRows[0].Cells[6].Value.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(StuId>0)

            {

                SqlCommand cmd = new SqlCommand("UPDATE demotable SET Name= @name, FatherName= @fathername, Age = @age, Gender =@gender,Address=@address, PhoneNo=@phone WHERE ID=@ID", con);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@name", textBox1.Text);
                cmd.Parameters.AddWithValue("@fathername", textBox2.Text);
                cmd.Parameters.AddWithValue("@age", textBox3.Text);
                cmd.Parameters.AddWithValue("@gender", comboBox1.Text);
                cmd.Parameters.AddWithValue("@address", textBox4.Text);
                cmd.Parameters.AddWithValue("@phone", textBox5.Text);
                cmd.Parameters.AddWithValue("@ID",this.StuId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("  Student information is been upadted ", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                GetStudentRecord();
                ResetFormControls();
            }
            else { MessageBox.Show(" Please select an student to update the information   ", "Select", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (StuId>0)
            {
                SqlCommand cmd = new SqlCommand("DELETE demotable WHERE ID=@ID", con);
                cmd.CommandType = CommandType.Text;
                
                cmd.Parameters.AddWithValue("@ID", this.StuId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("  Student information is Deleted ", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                GetStudentRecord();
                ResetFormControls();
            }
            else { 
                MessageBox.Show(" Please select an student to delete", "Select", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
